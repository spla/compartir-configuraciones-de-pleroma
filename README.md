Instrucciones:
---------------

Si quieres colaborar, puedes subir un archivo a `/bloqueos/`, usando el siguiente formato: `dominio.com.md` (puedes usar de plantilla alguna otra ya subida).

+ Recuerda indicar el motivo del bloqueo de cada instancia.
+ Indica la fecha de laúltima actualización.

En el directorio bloqueos se puede crear un fichero con la configuración de cada servidor. Es importante que indiques el motivo del bloqueo.

Servidores añadidos:
-------------------

+ pleroma.cat
+ pleroma.libretux.com
+ santsenques.com

Tipos de bloqueo:
-------------

+ **media_removal:** Instancias en las que eliminamos su contenido multimedia.
+ **media_nsfw:** Instancias donde su contenido multimedia será catalogado como NSFW(contenido sensible) por defecto.
+ **federated_timeline_removal:** Instancias que sus publicaciones serán eliminadas de nuestra línea de tiempo federada. Se podrá seguir a sus usuarios e interactuar con ellos.
+ **reject:** Instancias que serán totalmente expulsadas de nuestra federación.