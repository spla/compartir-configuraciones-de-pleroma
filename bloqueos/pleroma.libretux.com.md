*Motivo*
----------------

En libretux.com no creemos en el bloqueo de otras instancias y nos gustaría federar con todo el mundo. Pero según nuestras condiciones de uso, bloquearemos o moderaremos cualquier instancia que sea visible en nuestra "Toda la Red Conocida", que permita a sus usuarios publicar contenido prohibido en nuestra instancia.

Para ello, podremos aplicar alguna de estas reglas:

+ **media_removal:** Instancias en las que eliminamos su contenido multimedia.
+ **media_nsfw:** Instancias donde su contenido multimedia será catalogado como NSFW(contenido sensible) por defecto.
+ **federated_timeline_removal:** Instancias que sus publicaciones serán eliminadas de nuestra línea de tiempo federada. Se podrá seguir a sus usuarios e interactuar con ellos.
+ **reject:** Instancias que serán totalmente expulsadas de nuestra federación.

| Instancia          | Tipo de Bloqueo | Motivo                                            |
|:------------------:|:---------------:|:-------------------------------------------------:|
| humblr.social      | media_nsfw      | Usuarios que no marcan el contenido como sensible |
| vipgirlfriend.xxx  | media_nsfw      | Usuarios que no marcan el contenido como sensible |

_La lista será actualizada según se vayan detectando nuevos casos._


*Configuración (en config/prod.secret.exs)*
----------------------------------------

```elixir
config :pleroma, :instance,
    ...
    rewrite_policy: Pleroma.Web.ActivityPub.MRF.SimplePolicy

config :pleroma, :mrf_simple,
    media_nsfw: ["humblr.social", "vipgirlfriend.xxx"]
```

_última actualización: 26/05/2019_