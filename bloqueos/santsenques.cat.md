*Motivo*
----------------

| Instancia                    | Tipo de Bloqueo | Motivo                                      |
|:----------------------------:|:---------------:|:-------------------------------------------:|
| md.ggtea.org                 | reject          |                                             |
| humblr.social                | reject          |                                             |
| switter.at                   | reject          |                                             |
| kinky.business               | reject          |                                             |
| rubber.social                | reject          |                                             |
| kinkyelephant.com            | reject          |                                             |
| mstdn.jp                     | reject          |                                             |
| loli.estate                  | reject          |                                             |
| wxw.moe                      | reject          |                                             |
| social.homunyan.com          | reject          |                                             |
| baraag.net                   | reject          |                                             |
| freespeechextremist.com      | reject          |                                             |


*Configuración (en config/prod.secret.exs)*
----------------------------------------

```elixir
config :pleroma, :mrf_simple,
  reject: ['md.ggtea.org', "humblr.social", "switter.at", "kinky.business", "rubber.social", "kinkyelephant.com", "mstdn.jp", "loli.estate", "wxw.moe", "social.homunyan.com", "baraag.net", "freespeechextremist.com" ]

```

_última actualización: 26/05/2019_