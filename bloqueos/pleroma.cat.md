*Motivo*
------

Al principio de poner en marcha pleroma.cat se dió de alta un bot japonés que empezó a seguir a decenas
de usuarios de servidores japoneses.
Al día siguiente fue cuando me di cuenta que Toda La Red Conocida era japonesa, decenas de publicaciones en 
japonés.
Este es el motivo del bloqueo a unos cuantos servidores japoneses. No tengo nada en contra de ellos, que conste.

| Instancia                    | Tipo de Bloqueo | Motivo                                      |
|:----------------------------:|:---------------:|:-------------------------------------------:|
| freespeechextremist.com      | reject          | nazis                                       |
| knzk.me                      | reject          | usuarios seguidos por bot japonés nocturno  |
| vocalodon.net                | reject          | usuarios seguidos por bot japonés nocturno  |
| oransns.com                  | reject          | usuarios seguidos por bot japonés nocturno  |
| misskey.xyz                  | reject          | usuarios seguidos por bot japonés nocturno  |
| mstdn.beer                   | reject          | usuarios seguidos por bot japonés nocturno  |
| mstdn-workers.com            | reject          | usuarios seguidos por bot japonés nocturno  |
| unnerv.jp                    | reject          | usuarios seguidos por bot japonés nocturno  |
| mstdn.maud.io                | reject          | usuarios seguidos por bot japonés nocturno  |
| pawoo.net                    | reject          | usuarios seguidos por bot japonés nocturno  |
| mstdn.jp                     | reject          | usuarios seguidos por bot japonés nocturno  |
| friends.nico                 | reject          | usuarios seguidos por bot japonés nocturno  |
| imastodon.net                | reject          | usuarios seguidos por bot japonés nocturno  |
| mastodon.motcha.tech         | reject          | usuarios seguidos por bot japonés nocturno  |
| yakiniku.cloud               | reject          | usuarios seguidos por bot japonés nocturno  |
| digineko.jp                  | reject          | usuarios seguidos por bot japonés nocturno  |
| dotdon.jp                    | reject          | usuarios seguidos por bot japonés nocturno  |
| pokemon.mastportal.info      | reject          | usuarios seguidos por bot japonés nocturno  |
| flower.afn.social            | reject          | usuarios seguidos por bot japonés nocturno  |
| qiitadon.com                 | reject          | usuarios seguidos por bot japonés nocturno  |
| social.super-niche.club      | reject          | usuarios seguidos por bot japonés nocturno  |
| mstdn.kemono-friends.info    | reject          | usuarios seguidos por bot japonés nocturno  |
| shiroganedon.net             | reject          | usuarios seguidos por bot japonés nocturno  |
| tegedon.net                  | reject          | usuarios seguidos por bot japonés nocturno  |
| mstdn.glorificatio.org       | reject          | usuarios seguidos por bot japonés nocturno  |
| under-bank.blue              | reject          | usuarios seguidos por bot japonés nocturno  |
| mstdn.y-zu.org               | reject          | usuarios seguidos por bot japonés nocturno  |
| pritter.work                 | reject          | usuarios seguidos por bot japonés nocturno  |
| itabashi.0j0.jp              | reject          | usuarios seguidos por bot japonés nocturno  |

*Configuración (en config/prod.secret.exs)*
----------------------------------------

```elixir
config :pleroma, :instance,
    ...
    rewrite_policy: Pleroma.Web.ActivityPub.MRF.SimplePolicy

config :pleroma, :mrf_simple,
    reject: ["freespeechextremist.com","knzk.me","vocalodon.net","oransns.com","misskey.xyz","mstdn.beer","mstdn-workers.com","unnerv.jp","mstdn.maud.io","pawoo.net","mstdn.jp",
             "friends.nico","imastodon.net","mastodon.motcha.tech","yakiniku.cloud","digineko.jp","dotdon.jp","pokemon.mastportal.info","flower.afn.social",
             "qiitadon.com","social.super-niche.club","mstdn.kemono-friends.info","shiroganedon.net","tegedon.net","mstdn.glorificatio.org","under-bank.blue",
             "mstdn.y-zu.org","pritter.work","itabashi.0j0.jp"],
```

_última actualización: 26/05/2019_